<?php
/**
 * Created by PhpStorm.
 * User: Opa
 * Date: 19.07.2017
 * Time: 15:17
 */

namespace WebWikinger\Tools\SKUInitializer;

require_once __DIR__ . '/../../../../vendor/autoload.php';
require_once __DIR__ . '/../../../../autoload.php';


use WebWikinger\Tools\SKUInitializer\Model\ExternalItem;
use PHPUnit\Framework\TestCase;
use WebWikinger\PlentyMarketRest\PlentyMarketRest;

class SKUInitializerTest extends TestCase
{

    const URL = 'http://24917aa8c1.plentymarkets-x2.com/rest';
    const USERNAME = 'Dennis';
    const PASSWORD = '123Superpasswort';

    /**
     * @var $skuTool SKUInitializer
     */
    public static $skuTool;

    /**
     * @var $restClient PlentyMarketRest
     */
    public static $restClient;

    public static function setUpBeforeClass()
    {
        self::$skuTool = new SKUInitializer(SKUInitializerTest::URL, SKUInitializerTest::USERNAME, SKUInitializerTest::PASSWORD);
        self::$restClient = new PlentyMarketRest(SKUInitializerTest::URL, SKUInitializerTest::USERNAME, SKUInitializerTest::PASSWORD);
    }

    public function testSynchronisation()
    {
        $sourceOne = new ExternalItem('1234', '1234567890123', 12); // 102-1000
        $sourceTwo = new ExternalItem('1234', '1279370123333', 12); // 103-1001
        $dataSource = [$sourceOne, $sourceTwo];

        self::$skuTool->setDataSource($dataSource);
        self::$skuTool->setOverwriteMode(true);
        self::$skuTool->synchronize();


        // Todo: check for array subset instead of iterating
        // assertArraySubset(object, xyz);
        $skus = self::$restClient->listSKUs(1000);
        $skuOneSet = null;
        foreach ($skus as $sku) {
            if ($sku->sku === $sourceOne->getSkuValue()) {
                $skuOneSet = $sku->sku;
                break;
            }
        }

        $skus = self::$restClient->listSKUs(1001);
        $skuTwoSet = null;
        foreach ($skus as $sku) {
            if ($sku->sku === $sourceOne->getSkuValue()) {
                $skuTwoSet = $sku->sku;
                break;
            }
        }

        $this->assertEquals($sourceOne->getSkuValue(), $skuOneSet);
        $this->assertEquals($sourceOne->getMarketId(), 12);
        $this->assertEquals($sourceOne->getSkuValue(), $skuTwoSet);
        $this->assertEquals($sourceOne->getMarketId(), 12);
    }
}
