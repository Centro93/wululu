<?php
spl_autoload_register(function ($class) {
    if (strpos($class, "WebWikinger\\") === 0) {
        require __DIR__ . "\\src\\{$class}.php";
    }
});
