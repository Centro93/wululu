<?php


namespace WebWikinger\Tools\SKUInitializer;

use WebWikinger\Tools\SKUInitializer\Model\ExternalItem;
use WebWikinger\PlentyMarketRest\PlentyMarketRest;

/**
 * Tool for generating SKUs from an external source in plentymarkets
 * Class SKUInitializer
 * @package Tools\SKUInitializer
 */
class SKUInitializer
{

    /**
     * Restclient - Connection to PlentyMarkets REST API
     * @var PlentyMarketRest
     */
    private $rest;

    /**
     * Array of external items that needs to be processed
     * @var ExternalItem[]
     */
    private $dataSource;


    /**
     * Defines if Plentymarkets SKUs should be overwritten
     * @var string
     */
    private $overwrite = false;


    /**
     * SKUInitializer constructor.
     * @param $url
     * @param $username
     * @param $password
     */
    public function __construct($url, $username, $password)
    {
        $this->rest = new PlentyMarketRest($url, $username, $password);
    }

    public function setDataSource($dataSource)
    {
        $this->dataSource = [$dataSource];

        if (is_array($dataSource)) {
            $this->dataSource = $dataSource;
        }
    }

    public function setOverwriteMode($active)
    {
        $this->overwrite = $active;
    }

    public function synchronize()
    {
        /**
         * @var $externalItem ExternalItem
         */
        foreach ((array)$this->dataSource as $externalItem) {

            // Item is assigned to PlentyMarkets

            if ($externalItem->isAssigned()) { // todo: handle overwrite case
                $this->rest->createSKU(
                    $externalItem->getPlentyMarketsItemId(),
                    $externalItem->getPlentyMarketsVariationId(),
                    $externalItem->getSkuValue()
                );
            } else {
                $variation = $this->rest->searchVariations([$externalItem->getMatchingKey() => $externalItem->getMatchingValue()])->entries[0];

                if ($variation !== null) {
                    $options['sku'] = $externalItem->getSkuValue();

                    // Write new SKU - if overwrite on, check if SKU is set.
                    if (!$this->overwrite || empty($variation->sku)) {
                        print_r(
                            $this->rest->createSKU(
                                $variation->itemId,
                                $variation->id,
                                $externalItem->getMarketId(),
                                $externalItem->getAccountId(),
                                $options)
                        );
                    }
                } else {
                    continue;
                }
            }
        }
    }
}