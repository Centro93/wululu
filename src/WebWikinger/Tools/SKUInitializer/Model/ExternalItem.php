<?php

namespace WebWikinger\Tools\SKUInitializer\Model;
/**
 * Used as adapter class for the SKUInitializer. This is used to adapt an external data source.
 * Class ExternalItem
 */
class ExternalItem
{
    /**
     * @var string
     */
    private $skuValue;
    /**
     * @var string
     */
    private $matchingValue;

    /**
     * @var string
     */
    private $marketId;

    /**
     * @var string
     */
    private $accountId = '0';

    /**
     * @var string
     */
    private $matchingKey = 'barcode';


    //// Plentymarkets Assignments
    /**
     * @var string
     */
    private $plentyMarketsVariationId;
    /**
     * @var string
     */
    private $plentyMarketsItemId;

    public function __construct($skuValue, $matchingValue, $marketId)
    {
        $this->skuValue = $skuValue;
        $this->matchingValue = $matchingValue;
        $this->marketId = $marketId;
    }


    public function isAssigned()
    {
        return (!empty($this->plentyMarketsItemId) && !empty($this->plentyMarketsVariationId));
    }

    public function setPlentyMarketsVariationId($variationId)
    {
        $this->plentyMarketsVariationId = $variationId;
    }

    public function getPlentyMarketsVariationId()
    {
        return $this->plentyMarketsVariationId;
    }

    public function setPlentyMarketsItemId($itemId)
    {
        $this->plentyMarketsItemId = $itemId;
    }

    public function getPlentyMarketsItemId()
    {
        return $this->plentyMarketsItemId;
    }

    public function getMatchingValue()
    {
        return $this->matchingValue;
    }

    public function getSkuValue()
    {
        return $this->skuValue;
    }

    public function getMatchingKey()
    {
        return $this->matchingKey;
    }

    public function setMatchingKey(string $matchingKey)
    {
        $this->matchingKey = $matchingKey;
    }


    public function getMarketId()
    {
        return $this->marketId;
    }

    public function setMarketId(string $marketId)
    {
        $this->marketId = $marketId;
    }

    public function getAccountId()
    {
        return $this->accountId;
    }

    public function setAccountId(string $accountId)
    {
        $this->accountId = $accountId;
    }
}