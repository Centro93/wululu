<?php

namespace WebWikinger\PlentyMarketRest\Models;

use WebWikinger\PlentyMarketRest\PlentyMarketRest;

class Variation implements RestObject
{


    public $id = '';
    public $itemId = '';
    public $mainVariationId = '';
    public $isActive = true;
    public $isMain = false;
    public $number = '';
    public $variationCategories = [];

    public $unit = [
        'unitId'  => 0,
        'content' => 0,
    ];

    public function addCategoryId($id)
    {
        $this->variationCategories[] = ['categoryId' => $id];
    }


    public function save(PlentyMarketRest $restService)
    {
        // TODO: Implement save() method.
    }
}