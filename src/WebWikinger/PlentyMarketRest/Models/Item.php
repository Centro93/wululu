<?php

namespace WebWikinger\PlentyMarketRest\Models;


use WebWikinger\PlentyMarketRest\PlentyMarketRest;

class Item implements RestObject
{


    /**
     * @var $texts ItemText[]
     */
    public $texts;

    /**
     * @var $variations Variation[]
     */
    public $variations = [];

    // todo: much attributes missing

    public function __construct()
    {
    }

    public function addText(ItemText $itemText)
    {
        $this->texts[] = $itemText;
    }

    public function addVariation(Variation $variation)
    {
        $this->variations[] = $variation;
    }


    public function save(PlentyMarketRest $restService)
    {
        // TODO: Implement save() method.
    }
}