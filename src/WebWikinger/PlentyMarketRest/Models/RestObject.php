<?php

namespace WebWikinger\PlentyMarketRest\Models;

use WebWikinger\PlentyMarketRest\PlentyMarketRest;

interface RestObject
{
    public function save(PlentyMarketRest $rest);

}