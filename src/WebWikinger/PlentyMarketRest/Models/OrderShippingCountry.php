<?php

namespace WebWikinger\PlentyMarketRest\Models;

use WebWikinger\PlentyMarketRest\PlentyMarketRest;


/**
 * Class OrderShippingCountry
 * @property PlentyMarketRest rest
 * @property array            countries
 * @package App\Library\PlentyMarketRest
 */
class OrderShippingCountry implements RestObject
{
    public function __construct(PlentyMarketRest $rest)
    {
        $this->rest = $rest;
        $this->countries = $this->rest->getShippingCountries();
    }

    public function getById($id)
    {
        foreach ($this->countries as $country) {
            if ($country->id === $id) {
                return $country;
            }
        }

        return null;
    }

    public function getByIsoCode2($iso)
    {
        foreach ($this->countries as $country) {
            if ($country->isoCode2 === $iso) {
                return $country;
            }
        }

        return null;
    }

    public function getByIsoCode3($iso)
    {
        foreach ($this->countries as $country) {
            if ($country->isoCode3 === $iso) {
                return $country;
            }
        }

        return null;
    }

    public function getShippingCountries()
    {
        return $this->countries;
    }

    public function save(PlentyMarketRest $rest)
    {
        // TODO: Implement save() method.
    }
}