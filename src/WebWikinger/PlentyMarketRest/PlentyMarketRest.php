<?php

namespace WebWikinger\PlentyMarketRest;

/**
 * Class PlentyMarketRestService
 * @package App\Library\PlentyMarketRest
 */
class PlentyMarketRest extends RestClient
{

    /**
     * Path to configuration file - holds api settings
     */
    const CONFIGPATH = __DIR__ . '/config.txt';

    /**
     * PlentyMarketRestService constructor.
     * @param       $url string
     * @param       $name string
     * @param       $pass string
     * @internal param Authentification $auth
     */
    public function __construct($url, $name, $pass)
    {
        //create auth class
        //$this->auth = new Authentification(PlentyMarketRest::CONFIGPATH, parent::class);

        //read user auth credentials
        $user = unserialize(file_get_contents(PlentyMarketRest::CONFIGPATH));
        if (empty($user))
            $user = new \stdClass();

        parent::__construct(
            [
                'base_url'     => $url,
                'curl_options' => [CURLOPT_SSL_VERIFYPEER => false],
            ]
        );

        // construct rest client with saved token/ auth if available and the same as argument input
        // or if no saved auth is available, create new auth from arguments
        // or check if arguments and saved auth are the same and token is young enough, else get new token
        if (empty($user->plenty_url) || empty($user->plenty_pass) || empty($user->plenty_user) || empty($user->token) || empty($user->token_timestamp)
            || $user->plenty_user !== $name || $user->plenty_url !== $url || $user->plenty_pass !== $pass || !($user->token_timestamp < time() - 24 * 3600)) {

            $user->plenty_url = $url;
            $user->plenty_pass = $pass;
            $user->plenty_user = $name;
            $user->token = "";
            $user->token_timestamp = "";

            $req = $this->post('login', ['username' => $name, 'password' => $pass]);

            if (!empty($req->info->http_code) && $req->info->http_code === 200) {
                $req = $req->decode_response();
                //check if login was succesful
                if (!empty($req->accessToken)) {

                    //save new token and credentials
                    $user = new \stdClass();
                    $user->token = "{$req->tokenType} {$req->accessToken}";
                    $user->token_timestamp = time();
                }
            }
            file_put_contents(self::CONFIGPATH, serialize($user));
        }

        if (!empty($user->token))
            $this->options['headers'] = ['Authorization' => $user->token];
    }


    public function listClients()
    {
        return $this->get('webstores')->decode_response();
    }

    ////////// REST CALLS

    /// BATCH

    private function batch(array $calls) {
        $results = [];

        echo "pages: ". sizeof($calls)."</br>";
        echo json_encode($calls);


        //send calls in packages of 20 (plenty limitation)
        for($i = 0; $i <= sizeof($calls); $i += 20) {
            $length = $i + 20 <= sizeof($calls) ? 20 : sizeof($calls) - $i;
            $callsToSend = array_splice($calls, $i, $length);
            echo "calls to send: ".sizeof($callsToSend)."</br>";
            echo json_encode($callsToSend);
            $result = $this->post('batch', ['payloads' => $callsToSend])->decode_response();
            echo json_encode($result);
            array_push($results, $result);
        }

        return $results;
    }

    ////// ITEMS

    public function searchAllItems(array $options = [])
    {
        $options['page'] = isset($options['page']) ? $options['page'] : 1;
        $output = [];

        do {
            $res = $this->searchItems($options);
            if (!empty($res->entries)) {
                $output = array_merge($output, $res->entries);
            }
            $options['page']++;
        } while (!$res->isLastPage);

        return $output;
    }

    public function searchItemsBatch(array $options = []) {
        $results = [];

        //first call without batch to get page number
        $options = array_merge($options, ['page' => 1]);
        $pages = $this->searchItems($options)->lastPageNumber;

        //create batch calls
        $calls = [];
        for($i = 1; $i <= $pages; $i++) {
            $options = array_merge($options, ['page' => $i]);
            $call = [
                'resource' => 'rest/items',
                'method' => 'GET',
                'body' => $options
            ];
            array_push($calls, $call);
        }
        $results = $this->batch($calls);

        return $results;
    }

    public function searchItems(array $options = [])
    {
        return $this->get('items', $options)->decode_response();
    }

    public function showItems($itemId, array $options = [])
    {
        return $this->get("items/$itemId", $options)->decode_response();
    }

    public function createItem(array $options = [])
    {
        return $this->post('items', $options)->decode_response();
    }


    /**
     * @param array $options
     * @return mixed|null
     */
    public function searchItemManufacturer(array $options = [])
    {
        return $this->get('items/manufacturers', $options)->decode_response();
    }

    public function getAllItemManufacturer(array $options = [])
    {
        $options['page'] = isset($options['page']) ? $options['page'] : 1;
        $output = [];

        do {
            $res = $this->searchItemManufacturer($options);
            if (!empty($res->entries)) {
                $output = array_merge($output, $res->entries);
            }
            $options['page']++;
        } while (!$res->isLastPage);

        return $output;
    }


    ////// BARCODES

    public function getAllBarcodes()
    {
        return $this->get('barcodes')->decode_response();
    }


    ////// VARIATIONS
    ///
    public function searchAllVariations(array $options = [])
    {
        $options['page'] = isset($options['page']) ? $options['page'] : 1;
        $variations = [];

        do {
            $res = $this->searchVariations($options);
            if (!empty($res->entries)) {
                $variations = array_merge($variations, $res->entries);
            }
            $options['page']++;
        } while (!$res->isLastPage);

        return $variations;
    }

    public function searchVariations(array $options = [])
    {
        return $this->get('items/variations', $options)->decode_response();
    }

    //itemId is not relevant and can be set with an arbitrary number
    public function getVariation($variationid, array $options = null, $itemid = 666)
    {
        return $this->get("items/{$itemid}/variations/{$variationid}", $options)->decode_response();
    }

    public function getVariations($itemid, array $options = [])
    {
        return $this->get("items/{$itemid}/variations", $options)->decode_response();
    }

    public function getVariationProperties($itemid, $variationId, array $options = [])
    {
        return $this->get("items/{$itemid}/variations/{$variationId}/variation_properties",
            $options)->decode_response();
    }

    public function createVariation($itemId, array $options = [])
    {
        return $this->post("items/{$itemId}/variations", $options);
    }

    public function getVariationDescription($variationId, $lang = 'de')
    {
        return $this->get("items/666/variations/{$variationId}/descriptions/" . $lang)->decode_response();
    }

    public function getVariationWarehouses($variationId, $itemId = 666) {
        return $this->get("items/{$itemId}/variations/{$variationId}/variation_warehouses")->decode_response();
    }

    /// MARKET IDENT NUMBER

    public function listMarketIdentNumbers($variationId, array $options = [], $itemId = 666)
    {
        return $this->get("items/{$itemId}/variations/{$variationId}/market_ident_numbers", $options)->decode_response();
    }

    public function setMarketIdentNumbers($variationId, array $options = [], $itemId = 666)
    {
        return $this->post("items/{$itemId}/variations/{$variationId}/market_ident_numbers", $options)->decode_response();
    }

    //// SKU

    public function listSKUs($variationId, array $options = [], $itemId = 666)
    {
        return $this->get("items/{$itemId}/variations/{$variationId}/variation_skus", $options)->decode_response();
    }

    public function getSKU($itemId, $variationId, $skuId, array $options = [])
    {
        return $this->get("items/{$itemId}/variations/{$variationId}/variation_skus/{$skuId}", $options)->decode_response();
    }

    public function createSKU($itemId, $variationId, $marketId = 0, $accountId = 0, array $options = [])
    {
        $options['marketId'] = $marketId;
        $options['accountId'] = $accountId;

        return $this->post("items/{$itemId}/variations/{$variationId}/variation_skus", $options)->decode_response();
    }


    ////// ORDERS
    public function listOrders($options)
    {

        $i = 1;
        $output = [];
        $options = array_merge($options, ['page' => $i]);

        do {
            $options['page'] = $i;
            $orders = $this->get('orders', $options)->decode_response();
            if (!empty($orders->entries)) {
                $output = array_merge($output, $orders->entries);
            }
            ++$i;
        } while (!$orders->isLastPage);

        return $output;
    }

    //get all informations about order and customer
    public function getCompleteOrders(
        $getAddress = 0,
        $getPayment = false,
        $getShipping = false,
        $getVariation = false,
        $from = null,
        $till = null,
        $statusFrom = null,
        $statusTo = null,
        $orderType = null
    )
    {
        $orders = [];
        $variationBuffer = [];
        $i = 1;

        //build options
        $options = [];
        if (!empty($from) && !empty($till)) {
            $options['createdAtFrom'] = $from;
            $options['createdAtTill'] = $till;
        }

        if (!empty($statusFrom) && !empty($statusTo)) {
            $options['statusFrom'] = $statusFrom;
            $options['statusTo'] = $statusTo;
        }

        if (!empty($orderType)) {
            $options['typeId'] = $orderType;
        }

        do {
            //set current page
            $options['page'] = $i;
            $currentOrder = $this->get('orders', $options)->decode_response();

            foreach ((array)$currentOrder->entries as $order) {

                if ($getAddress !== 0) {
                    //search relations for customerid of receiver
                    foreach ((array)$order->relations as $orderRelation) {
                        if ($orderRelation->relation === 'receiver') {
                            $order->customerId = $orderRelation->referenceId;
                        }
                    }

                    //add address to order
                    $address = $this->getOrderAddress($order->id, $getAddress);

                    $order->address = $address;
                }

                if ($getPayment) {
                    //get payment information
                    $payment = $this->getPaymentInformation($order->id);
                    if ($payment) {
                        $order->payment = $payment;
                    }
                }

                if ($getShipping) {
                    $order->shipping = $this->getShippingInformation($order->id, []);
                }

                if ($getVariation) {
                    foreach ((array)$order->orderItems as $item) {
                        $variationId = $item->itemVariationId;

                        if ($variationId === 0) {
                            continue 2;
                        }

                        if (!empty($variationBuffer[$variationId])) {
                            $item->variation = $variationBuffer[$variationId];
                        } else {
                            $item->variation = $this->getVariation($variationId);
                            $variationBuffer[$variationId] = $item->variation;
                        }
                    }
                }

                $orders[] = $order;
            }
            ++$i;
        } while (!$currentOrder->isLastPage);

        return $orders;
    }

    public function getOrder($orderId, array $options = [])
    {
        return $this->get('orders/' . $orderId, $options)->decode_response();
    }

    public function getOrderAddress($orderId, $addressType)
    {
        return $this->get('orders/' . $orderId . '/addresses/' . $addressType)->decode_response();
    }

    public function addOrder($options)
    {
        return $this->post('orders', $options)->decode_response();
    }


    public function updateOrder($orderId, array $options = [])
    {
        return $this->put('orders/' . $orderId, $options)->decode_response();
    }


    public function createOrderProperty($orderId, $typeId, $value)
    {
        return $this->put("orders/{$orderId}/properties", ['typeId' => $typeId, 'value' => $value])->decode_response();
    }

    public function updateOrderProperty($orderId, $typeId, $value)
    {
        return $this->put("orders/{$orderId}/properties/{$typeId}", ['value' => $value])->decode_response();
    }

    public function createPayment($options)
    {
        return $this->post('payments', $options)->decode_response();
    }

    public function updatePayment($options)
    {
        return $this->put("payments", $options)->decode_response();
    }

    public function createPaymentOrderRelation($paymentId, $orderId)
    {
        return $this->post("payment/{$paymentId}/order/{$orderId}", [])->decode_response();
    }

    public function createPaymentContactRelation($paymentId, $contactId)
    {
        return $this->post("payment/{$paymentId}/contact/{$contactId}", [])->decode_response();
    }


    /**
     * @param array $orderIds = Array containing Id's of orders
     * @param float $state = State to which the orders have to be set
     */
    public function updateOrderStates(array $orderIds, $state)
    {
        $options = [
            'statusId' => $state,
        ];

        foreach ($orderIds as $orderId) {
            $this->put("orders/{$orderId}", $options);
        }
    }

    /////PAYMENT

    public function getPaymentInformation($orderId)
    {
        return $this->get("payments/orders/{$orderId}")->decode_response();
    }


    // SHIPPING PROVIER
    public function getShippingProvider($id = null, array $options = [])
    {
        return $this->get("orders/shipping/shipping_service_providers/{$id}", $options)->decode_response();
    }

    // SHIPPING PROVIER
    public function getShippingProfile($id = null, array $options = [])
    {
        return $this->get("orders/shipping/presets/{$id}", $options)->decode_response();
    }

    public function listShippingPackages($orderId, array $options = [])
    {
        return $this->get("orders/{$orderId}/shipping/packages", $options)->decode_response();
    }

    public function createShippingPackage($orderId, array $options = [])
    {
        return $this->post("orders/{$orderId}/shipping/packages", $options)->decode_response();
    }

    public function updateShippingPackage($orderId, $orderShippingPackageId, array $options = [])
    {
        return $this->put("orders/{$orderId}/shipping/packages/" . $orderShippingPackageId,
            $options)->decode_response();
    }

    public function deleteAllShippingPackages($orderId, array $options = [])
    {
        $this->delete("orders/{$orderId}/shipping/packages/", $options);
    }

    public function getShippingInformation($orderId, array $options = [])
    {
        return $this->get("orders/{$orderId}/shipping/shipping_information", $options)->decode_response();
    }

    public function createShippingInformation($orderId, array $options = [])
    {
        /*
         * {
         * "orderId": 107,
         * "shippingServiceProvider": "Dpd Cloud Webservice",
         * "transactionId": "",
         * "shippingStatus": "registered",
         * "shippingCosts": "0.00",
         * "additionalData": {
         * "ShipService": "Classic",
         * "Reference1": "john@account.com",
         * "Reference2": "Order: 107"
         * },
         * "registrationAt": "2016-12-23T11:11:00+01:00",
         * "shipmentAt": "2016-12-23T00:00:00+01:00"
         * }
         *
         */

        $options['orderId'] = $orderId;

        return $this->post('orders/shipping/shipping_information', $options)->decode_response();
    }

    public function getShippingCountries()
    {
        return $this->get('orders/shipping/countries')->decode_response();
    }


////// STOCKS


    public function listItemVariationStock($itemId, $variationId, array $options = [])
    {
        return $this->get("items/{$itemId}/variations/{$variationId}/stock", $options)->decode_response();
    }

    public function listItemVariationStockPerStorageLocations($itemId, $variationId, array $options = [])
    {
        return $this->get("items/{$itemId}/variations/{$variationId}/stock/storageLocations",
            $options)->decode_response();
    }

    public function correctStock(
        $itemId,
        $variationId,
        $quantity,
        $warehouseId,
        $storageLocationId,
        array $options = []
    )
    {
        $options['quantity'] = $quantity;
        $options['warehouseId'] = $warehouseId;
        $options['storageLocationId'] = $storageLocationId;

        return $this->put("items/{$itemId}/variations/{$variationId}/stock/correction", $options)->decode_response();
    }


////// STOCKMANAGEMENT
    public function correctStockManagement($warehouseId, array $options = [])
    {
        /*
         * \VariationStockCorrection[]
         * $variationId         Int     *req.
         * $quantity            Float   *req.
         * $storageLocationId   Int     *req.
         * $reasonId            Int
         */

        return $this->put("stockmanagement/warehouses/{$warehouseId}/stock/correction", $options);
    }

    public function bookIncomingStock($warehouseId, $options)
    {
        return $this->put('stockmanagement/warehouses/' . $warehouseId . '/stock/bookIncomingItems', $options)->decode_response();
    }

    public function getStockByWarehouse($warehouseId, $options = [])
    {
        $stockArray = [];
        $i = 0;

        do {
            array_merge($options, ['page' => $i]);

            $stock = $this->get('stockmanagement/warehouses/' . $warehouseId . '/stock', $options)->decode_response();
            $i++;

            if (!empty($stock->entries)) {
                $stockArray[] = $stock->entries;
            } else {
                break;
            }
        } while (!$stock->isLastPage);

        return $stockArray;
    }

////// ACCOUNTS

    public function listAccounts(array $options = [])
    {
        return $this->get('accounts/', $options)->decode_response();
    }

    public function getContacts($accountId, array $options = [])
    {
        return $this->get("accounts/{$accountId}/contacts", $options)->decode_response();
    }

    public function listContacts(array $options = [])
    {
        return $this->get('accounts/contacts/', $options)->decode_response();
    }

    public function getContact($id, array $options = [])
    {
        return $this->get('accounts/contacts/' . $id, $options)->decode_response();
    }

    public function addContact(array $options = [])
    {
        return $this->post('accounts/contacts', $options)->decode_response();
    }


    public function addAddress(array $options = [])
    {
        return $this->post('accounts/addresses', $options)->decode_response();
    }

    public function addContactAddress($contactId, array $options = [])
    {
        return $this->post("accounts/contacts/{$contactId}/addresses", $options)->decode_response();
    }



    // CONTACT OPTION

    // Type Ids
    /*
    1 = Telephone
    2 = Email
    3 = Telefax
    4 = Web page
    5 = Marketplace
    6 = Identification number
    7 = Payment
    8 = User name
    9 = Group
    10 = Access
    11 = Additional
     */


    public function getAllContactOptions($contactId, array $options = [])
    {
        return $this->get("accounts/contacts/{$contactId}/options", $options)->decode_response();
    }

    public function addContactOption($contactId, array $options = [])
    {

        return $this->post("accounts/contacts/{$contactId}/options", $options)->decode_response();
    }

    public function updateContactOption($contactId, array $options = [])
    {

        return $this->put("accounts/contacts/{$contactId}/options", $options)->decode_response();
    }


////// DYNAMISCHER EXPORT

    public function listExports(array $options = [])
    {
        return $this->get('exports', $options)->decode_response();
    }

    public function getExport($id, $options)
    {
        return $this->get('exports/' . $id, $options)->decode_response();
    }

}