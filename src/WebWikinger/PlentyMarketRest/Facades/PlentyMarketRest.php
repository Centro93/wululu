<?php

namespace WebWikinger\PlentyMarketRest\Facades;

use Illuminate\Support\Facades\Facade;

class PlentyMarketRest extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'plenty'; }

}