<?php
/**
 * Created by IntelliJ IDEA.
 * User: peter
 * Date: 10.07.2017
 * Time: 16:07
 */

namespace WebWikinger\PlentyMarketRest\Helpers;


use App\User;
use Psr\Log\LogLevel;

class Logger extends \Katzgrau\KLogger\Logger
{
    private $user;

    function __construct(User $user, $name)
    {
        $this->user = $user;
        parent::__construct(storage_path("/logs/$name"), LogLevel::INFO, ["filename" => "$name.txt"]);
    }

    public function info($message, array $context = array())
    {
        if($this->user->logging)
            parent::info($message, $context);
    }
}