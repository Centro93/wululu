<?php
/**
 * Created by PhpStorm.
 * User: jonas
 * Date: 6/1/17
 * Time: 4:48 PM
 */

namespace WebWikinger\PlentyMarketRest\Helpers;

use RuntimeException;

class PlentyHelper
{

    public function __construct()
    {
    }

    /*
     * $countryMapping = PlentyMarketRest->getShippingCountry()
     * $id = int
     */
    public function getCountryName(array $countryMapping, $id)
    {
        if (!empty($countryMapping)) {
            foreach ($countryMapping as $country) {
                if ($country->id === $id) {
                    return $country->name;
                }
            }
            throw new RuntimeException('Country Mapping Error');
        }

        throw new RuntimeException('Country Mapping Error');
    }


    //removes all parent orders if child orders are found
    public function removeParentOrders(array $orders): array
    {
        $parentOrderIds = [];

        //find all child orders
        foreach ($orders as $order) {
            foreach ((array)$order->orderReferences as $orderReference) {
                if ($orderReference->referenceType === 'parent') {
                    $parentOrderIds[] = $orderReference->originOrderId;
                    break;
                }
            }
        }

        //remove parent orders
        foreach ($orders as $key => $order) {
            if (array_has($parentOrderIds, $order->id)) {
                array_splice($orders, $key);
            }
        }

        return $orders;
    }
}