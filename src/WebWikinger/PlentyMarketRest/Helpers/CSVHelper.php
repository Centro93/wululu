<?php

namespace WebWikinger\PlentyMarketRest\Helpers;

class CSVHelper
{

    private $csvPath;

    public function __construct()
    {
        $this->csvPath = getcwd().'/../csv/';
    }


    public function createCSV($orders, $name)
    {

        $filePath = $this->csvPath . $name;

        $file = fopen($filePath, 'w');

        $headerSet = false;

        foreach ($orders as $order) {

            if (!$headerSet) {
                //print csv header
                fputcsv($file, array_keys($order));

                $headerSet = true;
            }

            fputcsv($file, $order);
        }

        fclose($file);

        return $filePath;
    }


    public function readCSV($fileName)
    {
        $csvArray = [];

        if (($handle = fopen($fileName, 'r')) !== false) {
            $headerRow = fgetcsv($handle, 1000, ';');
            while (($row = fgetcsv($handle, 1000, ';')) !== false) {
                array_push($csvArray, array_combine($headerRow, $row));
            }
        }

        return $csvArray;
    }

}