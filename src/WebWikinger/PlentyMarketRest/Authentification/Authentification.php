<?php
/**
 * Created by PhpStorm.
 * User: Opa
 * Date: 13.07.2017
 * Time: 15:33
 */

namespace WebWikinger\Authentificaton;


class Authentification
{
    public $plenty_url;
    public $plenty_pass;
    public $plenty_user;
    public $token_timestamp;
    public $token;

    private $configPath;
    private $config;

    private $restClient;

    public function __construct($configPath, $restClient)
    {
        $this->configPath = $configPath;
        $this->restClient = $restClient;
    }

    public function login($url, $user, $token)
    {
        $login = new $this->restClient(
            [
                'base_url' => $user->plenty_url,
                'curl_options' => [CURLOPT_SSL_VERIFYPEER => false],
            ]
        );

        // if token from yesterday or empty, get new token and save it
        if (empty($user->token_timestamp) || empty($user->token) || $user->token_timestamp < time()) {

            $req = $this->post('login',
                ['username' => $user->plenty_user, 'password' => $user->plenty_pass]);
            if ($req->response) {
                $req = $req->decode_response();
                //check if login was succesful
                if (!empty($req->accessToken)) {
                    //save new token
                    $user->token = "{$req->tokenType} {$req->accessToken}";
                    $user->token_timestamp = time() + $req->expiresIn;

                    file_put_contents($this->configPath, serialize($user));
                }
            }
        }
    }

    public function getToken($url, $user, $pass)
    {

    }

    public function setToken($token)
    {

    }

}
