<?php

namespace WebWikinger\PlentyMarketRest;

use Illuminate\Support\ServiceProvider;

class PlentyMarketRestServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        /*
        $this->app->singleton('plenty', function ($app) {
            return new PlentyMarketRest($app);
        });
*/
        $this->app->booting(function () {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('PlentyMarketRest', 'WebWikinger\PlentyMarketRest\Facades\PlentyMarketRest');
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('plenty');
    }
}
